import { TestBed, inject } from '@angular/core/testing';

import { DashboardLineService } from './dashboard-line.service';

describe('DashboardLineService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DashboardLineService]
    });
  });

  it('should be created', inject([DashboardLineService], (service: DashboardLineService) => {
    expect(service).toBeTruthy();
  }));
});
