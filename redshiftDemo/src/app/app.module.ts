import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { DashboardService } from './dashboard.service';


import { AppComponent } from './app.component';
import {DashboardLineService} from "./dashboard-line.service";


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [DashboardService, DashboardLineService],
  bootstrap: [AppComponent]
})
export class AppModule { }
