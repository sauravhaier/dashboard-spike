import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DashboardService {

  constructor(private _http: HttpClient) {
  }

  dataFromRedShift() {
    const header: HttpHeaders = new HttpHeaders;
    header.append('accept', '*/*');
    // adding CORS proxy for now. TODO: got to add your own proxy.conf.json SREE
    return this._http.get('https://cors.io/?https://zsqaai8pzk.execute-api.ap-south-1.amazonaws.com/dev/aggregatereport', {headers: header})
      .map(result => result);
  }
}
