import {Component, OnInit} from '@angular/core';
import {DashboardService} from './dashboard.service';
import {DashboardLineService} from "./dashboard-line.service";
import { Chart } from 'chart.js';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  chart = []; // This will hold our chart info
  chart1 = []; // This will hold our chart info
  constructor(private _dashboard: DashboardService, private _weather:DashboardLineService ) {}
  ngOnInit() {
    this.createBarChart();
    this.createLineChart();
  }
  createBarChart():Chart{
    this._dashboard.dataFromRedShift()
      .subscribe(res => {
        const docType = [];
        const beg01 = [];
        const beg02 = [];
        const beg03 = [];
        const beg01Style = [];
        const beg02Style = [];
        const beg03Style = [];
        for (let i = 0 ; typeof res[i] !== 'undefined' ; i++) {
          docType.push(res[i].doctype);
          beg01.push(res[i].beg01);
          beg01Style.push('#3e95cd');
          beg02.push(res[i].beg02);
          beg02Style.push('#8e5ea2');
          beg03.push(res[i].beg03);
          beg03Style.push('#3cba9f');
        }
        const chart_config = {
          type: 'bar',
          data: {
            labels: docType,
            datasets: [
              {
                label: 'beg01',
                data: beg01,
                backgroundColor: beg01Style,
                fill: true
              },
              {
                label: 'beg02',
                data: beg02,
                backgroundColor: beg02Style,
                fill: true
              },
              {
                label: 'beg03',
                data: beg03,
                backgroundColor: beg03Style,
                fill: true
              }
            ]
          },
          options: {
            legend: {
              display: true
            },
            title: {
              display: true,
              text: 'Redshift Data from RuleSet'
            },
            scales: {
              xAxes: [{
                categoryPercentage: 0.4,
                barPercentage: 0.7,
                display: true,
              }],
              yAxes: [{
                display: true,
                ticks: {
                  beginAtZero: true
                }
              }],
            },
            onClick: handleClick
          }
        };
        var canvas = <HTMLCanvasElement> document.getElementById("chart1");
        var ctx = canvas.getContext("2d");
        this.chart = new Chart(ctx, chart_config);
        function handleClick(evt) {
          const activeElement = this.chart.getElementAtEvent(evt);
          if(activeElement.length > 0){
            console.log(chart_config.data.labels[activeElement[0]._index]);
            console.log(chart_config.data.datasets[activeElement[0]._datasetIndex].data[activeElement[0]._index]);
          }
        }
        return this.chart;
      });
  }

  createLineChart():Chart {
    this._weather.dataFromRedShift()
      .subscribe(res => {
        let temp_max = res['list'].map(res => res.main.temp_max);
        let temp_min = res['list'].map(res => res.main.temp_min);
        let alldates = res['list'].map(res => res.dt)

        let weatherDates = []
        alldates.forEach((res) => {
          let jsdate = new Date(res * 1000)
          weatherDates.push(jsdate.toLocaleTimeString('en', {year: 'numeric', month: 'short', day: 'numeric'}))
        });

        const chart_configure = {
          type: 'line',
          data: {
            labels: weatherDates,
            datasets: [
              {
                data: temp_max,
                borderColor: "#3cba9f",
                fill: false
              },
              {
                data: temp_min,
                borderColor: "#ffcc00",
                fill: false
              },
            ]
          },
          options: {
            legend: {
              display: false
            },
            scales: {
              xAxes: [{
                display: true
              }],
              yAxes: [{
                display: true
              }],
            }
          }
        };
        var canvas1 = <HTMLCanvasElement> document.getElementById("chart2");
        var ctx1 = canvas1.getContext("2d");
        this.chart1 = new Chart(ctx1,chart_configure);
        return this.chart1;
      });
  }
}
